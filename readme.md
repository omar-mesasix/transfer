Transfer
===================


Transfer is a little library that allows you to transfer files from different file systems. For example, if you wan to transfer a file from your FTP server to your AWS S3 instance or vice versa, this library can help.

----------

Installation
-------------
You can do add or run a composer command, like so:

    composer require m6/transfer

Quick Start Guide
-------------
Let's transfer a file from an FTP server to AWS S3!

Before we can start transfering our files. We need to declare first the `from` and `to` filesystems like so:

**For FTP:**

    $ftp = new \M6\Transfer\Ftp('yourserver.com, 'your_ftp_username', 'your_ftp_password, ['root' => 'path_to_your_root_directory']);

**For S3:**

    $s3 = new \M6\Transfer\S3('your_s3_key', 'your_s3_secret', 'your_s3_region', 'your_s3_bucket');

Ok now that we already setup our file systems, we can now use them to transfer files from and to or vice versa. Let me show you:

**Let's instantiate first the Transfer class:**

    $transfer = new \M6\Transfer\Transfer;

**Then do this:**

    $done = $transfer->file('path_to_your_file')->from($ftp)->to($s3)->go();

If all are good, the transfer will return `true` else an `Exception` will be returned.
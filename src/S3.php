<?php

namespace M6\Transfer;

use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter as Adapter;
use League\Flysystem\Filesystem;

class S3 implements FileTransferInterface
{
    private $adapter;
    private $filesystem;

    public function __construct($key = '', $secret = '', $region = '', $bucketName = '', $optionalPrefix = '', $version = 'latest')
    {
        $client = S3Client::factory([
            'credentials' => [
                'key' => $key,
                'secret' => $secret,
            ],
            'region' => $region,
            'version' => $version,
        ]);

        $this->adapter = new Adapter($client, $bucketName, $optionalPrefix);
        $this->filesystem = new Filesystem($this->adapter);
    }

    /**
     * Get filesystem.
     *
     * @return Filesystem
     */
    public function filesystem()
    {
        return $this->filesystem;
    }

    /**
     * Get adapter.
     *
     * @return Adapter
     */
    public function adapter()
    {
        return $this->adapter;
    }
}

#END OF PHP FILE
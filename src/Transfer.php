<?php

namespace M6\Transfer;

use League\Flysystem\FileNotFoundException;

class Transfer
{
    private $to;
    private $from;
    private $pathToFile;

    public function __construct(FileTransferInterface $from = null, FileTransferInterface $to = null, $pathToFile = '')
    {
        $this->from = $from;
        $this->to = $to;
        $this->pathToFile = $pathToFile;
    }

    /**
     * Upload file.
     *
     * @param string $visibility
     * @param array $config
     * @return bool
     * @throws FileNotFoundException
     * @throws \HttpException
     */
    public function go($visibility = 'public', $config = [])
    {
        $this->checkVariables();

        $this->hasFile();

        // Get contents.
        $contents = $this->getContents();

        $transferConfig = ['visibility' => $visibility];
        $uploaded = $this->to->filesystem()->put($this->pathToFile, $contents, $transferConfig);

        if (!$uploaded) {
            $error = isset($config['error']) ? $config['error'] : 'Unable to upload file. Something is wrong.';
            throw new \HttpException($error, 400);
        }

        return true;
    }

    /**
     * Set to filesystem.
     *
     * @param FileTransferInterface $file
     * @return $this
     */
    public function to(FileTransferInterface $file)
    {
        $this->to = $file;

        return $this;
    }

    /**
     * Set from filesystem.
     *
     * @param FileTransferInterface $file
     * @return $this
     */
    public function from(FileTransferInterface $file)
    {
        $this->from = $file;

        return $this;
    }

    /**
     * Set file.
     *
     * @param string $pathToFile
     * @return $this
     */
    public function file($pathToFile = '')
    {
        $this->pathToFile = $pathToFile;

        return $this;
    }

    /**
     * Check for required variables first.
     *
     * @throws Exception
     */
    private function checkVariables()
    {
        if (!$this->pathToFile) {
            throw new Exception('Please provide file\'s path.', 400);
        }

        if (!$this->from) {
            throw new Exception('Don\'t know where to look for the file. Please provide the \'from\' filesystem.', 400);
        }

        if (!$this->to) {
            throw new Exception('Don\'t know where to transfer the file. Please provide the \'to\' filesystem.', 400);
        }
    }

    /**
     * Check if file is found.
     *
     * @return mixed
     * @throws FileNotFoundException
     */
    private function hasFile()
    {
        if (!$this->from instanceof FileTransferInterface) {
            throw new FileNotFoundException($this->pathToFile, 400);
        }

        $hasFile = $this->from->filesystem()->has($this->pathToFile);

        if (!$hasFile) {
            throw new FileNotFoundException($this->pathToFile, 400);
        }

        return $hasFile;
    }

    /**
     * Get contents.
     *
     * @return mixed
     * @throws \HttpException
     */
    private function getContents()
    {
        $contents = $this->from->filesystem()->read($this->pathToFile);

        if ($contents === false) {
            throw new \HttpException('Unable to get contents for file: ' . $this->pathToFile);
        }
        return $contents;
    }
}

#END OF PHP FILE
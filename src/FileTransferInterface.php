<?php

namespace M6\Transfer;

use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Filesystem;

interface FileTransferInterface
{
    /**
     * Get filesystem.
     *
     * @return Filesystem
     */
    public function filesystem();

    /**
     * Get adapter.
     *
     * @return AbstractAdapter
     */
    public function adapter();
}
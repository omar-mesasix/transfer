<?php

namespace M6\Transfer;

use League\Flysystem\Adapter\Ftp as Adapter;
use League\Flysystem\Filesystem;

class Ftp implements FileTransferInterface
{
    private $adapter;
    private $filesystem;

    public function __construct($host = '', $username = '', $password = '', $optional = [])
    {
        $optional = array_merge(['host' => $host, 'username' => $username, 'password' => $password], $optional);

        $this->adapter = new Adapter($optional);
        $this->filesystem = new Filesystem($this->adapter);
    }

    /**
     * Get filesystem.
     *
     * @return Filesystem
     */
    public function filesystem()
    {
        return $this->filesystem;
    }

    /**
     * Get adapter.
     *
     * @return Adapter
     */
    public function adapter()
    {
        return $this->adapter;
    }
}

#END OF PHP FILE